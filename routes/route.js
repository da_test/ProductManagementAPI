// import product routes
const productRoutes = require('./products');

const appRouter = (app, fs) => {

    // default route
    app.get('/', (req, res) => {
        res.send('Default Route');
    });

    // // other routes
    productRoutes(app, fs);

};

module.exports = appRouter;