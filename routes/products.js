const LocalStorage = require('node-localstorage').LocalStorage;

const productRoutes = (app, fs) => {

    // variables
    const dataPath = './data/products.json';
    const localStorage = new LocalStorage('./scratch');

    // to get product list 
    app.get('/products', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            res.send(data);
        });
    });

    // to get cart list 
    app.get('/cart', (req, res) => {
        res.send(readFromLocalStorage("cartItems"));
    });

    // to get cart item count  
    app.get('/cartItemCount', (req, res) => {
        res.send(readFromLocalStorage("cartItemCount"));
    });

    // to add to cart
    app.post('/cart', (req, res) => {
        const newCartItem = req.body;
        const cartItemList = readFromLocalStorage('cartItems') || [];
        cartItemList.push(newCartItem)
        writeToLocalStorage('cartItems', JSON.stringify(cartItemList))
        const cartItemCount = calculateCartItemCount()
        res.send(cartItemCount)
    });

    // to update quantity
    app.put('/cart', (req, res) => {
        const newCartItem = req.body;
        const cartItemList = readCartItems();
        const productIndex = cartItemList.findIndex(item => item.productId === newCartItem.productId);
        if (productIndex > -1) {
            cartItemList[productIndex].cartCount = newCartItem.cartCount;
        }
        const cartItemCount = calculateCartItemCount(cartItemList)
        res.send(cartItemCount)
    });

    // to delete an item from the cart
    app.delete('/cart/:productId', (req, res) => {
        const productId = req.params["productId"];
        const cartItemList = readFromLocalStorage("cartItems");
        cartItemList.splice(cartItemList.findIndex(item => item.productId === productId), 1);
        writeToLocalStorage('cartItems', JSON.stringify(cartItemList))
        const cartItemCount = calculateCartItemCount(cartItemList)
        res.send(cartItemCount)
    });


    // helper methods
    const readFromLocalStorage = (storageKey) => {
        return JSON.parse(localStorage.getItem(storageKey));
    }

    const writeToLocalStorage = (storageKey, storageValue) => {
        localStorage.setItem(storageKey, JSON.stringify(storageValue));
    }

    const calculateCartItemCount = (cartItemList = []) => {
        let cartItemCount = readFromLocalStorage('cartItemCount');
        if (cartItemList && cartItemList.length) {
            cartItemCount = 0;
            cartItemList.forEach((product) => {
                cartItemCount += parseInt(product.cartCount, 10);
            });
        }
        else {
            cartItemCount += 1;
        }
        writeToLocalStorage('cartItemCount', cartItemCount);
        return cartItemCount;
    }

};

module.exports = productRoutes;

